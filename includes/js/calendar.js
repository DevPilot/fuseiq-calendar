(function($) {

      Drupal.behaviors.fuseiq_calendar = {
        attach: function (context, settings) {
          if (context == document) {

          $(document).ready(function() {
            var todays_date = moment().format('YYYY-MM-DD');
            
            // Start the calendar Function
            $('#calendar').fullCalendar({
              header: {
                left: 'prev,next today',
                center: 'title',
                right: 'basicWeek,month'
              },
        
              // customize the button names,
              // otherwise they'd all just say "list"
              views: {
//                 listDay: { buttonText: 'list day' },
                basicWeek: { buttonText: 'week' }
              },
              defaultView: 'month',
              defaultDate: todays_date,
              //navLinks: true, // can click day/week names to navigate views
              editable: false,
              eventLimit: true, // allow "more" link when too many events
              events: '/events-feed',
            }); // END Calendar Function               
          });  
          } // END context.document
        } // END function
      };
})(jQuery);