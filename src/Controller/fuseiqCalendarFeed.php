<?php

namespace Drupal\fuseiq_calendar\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Url;

/**
 * Class fuseiqCalendarFeed.
 */
class fuseiqCalendarFeed extends ControllerBase {

  /**
   * Event Feed.
   */
  public function eventsFeed() {
    
    $data = [];
    
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'event')
      ->condition('status', 1);
    $nids = $query->execute();

    if (!empty($nids)) {
      $nodes = Node::loadMultiple($nids);

      if (!empty($nodes)) {

        foreach ($nodes as $node) {
          $date = $node->get('field_date')->getValue()[0];
          $options = ['absolute' => FALSE];
          $url = Url::fromRoute('entity.node.canonical', ['node' => $node->id()], $options);            
          $data[] = [
            'title' => $node->label(),
            'url' => $url->toString(),            
            'start' => $date['value'],
            'end' => $date['end_value'],
          ];
          if($date['end_value'] != NULL) {
/*
            $data[] = [
              'end' => $date['end_value'],
            ];
*/
          }
        }
      }
    }


    return new JsonResponse($data);
    //return $data;
  }

}
