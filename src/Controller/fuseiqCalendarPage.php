<?php

namespace Drupal\fuseiq_calendar\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class fuseiqCalendarPage.
 */
class fuseiqCalendarPage extends ControllerBase {

  /**
   * Calendar Page output.
   */
  public function content() {
    
    $year = date('Y',time()); 
	  
	  $output = array(
      '#calendar_options' => 'test',
      '#theme' => 'calendar_lists',
      '#attached' => array(
        'library' => array(
          'fuseiq_calendar/calendar',
        ),
      ),        
      
	  );   
      
    	return $output;
  }

}
