<?php

namespace Drupal\fuseiq_calendar\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Component\Plugin\PluginBase;
use Drupal\Core\Entity;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;


/**
 * Provides a 'Calendar' Block.
 *
 * @Block(
 *   id = "fuseiq_calendar_view",
 *   admin_label = @Translation("Calendar View of Events"),
 * )
 */
class calendarView extends BlockBase {

  /**
   * {@inheritdoc}
   */
     
  public function build() {	 
    
    $year = date('Y',time()); 
	  
	  $output = array(
      '#calendar_options' => 'test',
      '#theme' => 'calendar_lists',
      '#attached' => array(
        'library' => array(
          'fuseiq_calendar/calendar',
        ),
      ),        
      
	  );   
      
    	return $output;

    }
}
 